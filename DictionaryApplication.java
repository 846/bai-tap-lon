import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

public class DictionaryApplication extends DictionaryCommandline {
    private DictionaryCommandline dic = new DictionaryCommandline();
    private JPanel MainPanel;
    private JTextField txtSeacher;
    private JLabel Menu;
    private JButton buttonsearch;
    private JList wordList;
    private JLabel target;
    private JButton buttonback;

    public DictionaryApplication() throws IOException {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(MainPanel);
        frame.pack();
        frame.setVisible(true);
        dic.insertFromFile();

        List<String> tarList = wordList.getSelectedValuesList();
        buttonsearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String b = txtSeacher.getText();
                DefaultListModel<String> worddd  = new DefaultListModel<>();
                b = formWord(b);
                int check = 0;
                for(int i = 0; i< word.size(); i++){
                    String a = word.get(i).getWord_target();
                    boolean s = false;
                    s = miniSearch(a, b);
                    if(s){
                        check ++;
                        worddd.addElement(word.get(i).getWord_target()+ "                                                   " + word.get(i).getWord_explain() + "\n");
                    }
                }
                if(check == 0){
                    worddd.addElement("Không có từ này.");
                }
                wordList.setModel(worddd);
            }
        });




        DefaultListModel<String> wordd  = new DefaultListModel<>();
        for(int i = 0; i<word.size(); i++){
            String a = "";
            for(int j = 0; j< 80-word.get(i).getWord_target().length();j++){
                a += " ";
            }
            wordd.addElement(word.get(i).getWord_target() + a + word.get(i).getWord_explain());
        }

        wordList.setModel(wordd);

        buttonback.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                wordList.setModel(wordd);
            }
        });
    }
    public static void main(String[] args) throws IOException {
        new DictionaryApplication();

    }
}