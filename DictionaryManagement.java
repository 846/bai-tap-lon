import java.io.*;
import java.util.Scanner;

public class DictionaryManagement extends Dictionary {
    public void insertFromCommandline(){
        System.out.println("Nhập từ: ");
        Scanner sc = new Scanner(System.in);
        String target = sc.nextLine();
        target = formWord(target);
        boolean check = true;
        int x = 0;
        for(int i = 0; i< word.size(); i++){
            if(target.equals(word.get(i).getWord_target())){
                check = false;
                x = i;
            }
        }
        if(check){
        System.out.println("Nhập nghĩa: ");
        String explain = sc.nextLine();
        explain = formWord(explain);
        Word a = new Word(target, explain);
        word.add(a);
        } else {
            System.out.println("Đã có từ này, ý bạn là: " + word.get(x).getWord_explain());
        }
    }
    public void insertFromFile() throws IOException {
        File file = new File("dictionaries");
        Scanner s = new Scanner(file);
        String a;
        while (s.hasNextLine()){
            a = s.nextLine();
            String[] wor = new String[2];
            for(int x = 0; x < 2; x++){
                wor[x] = "";
            }
            Scanner sc = new Scanner(a).useDelimiter("   ");
            wor[0] = sc.next();
            wor[1] = sc.next();

            Word neword = new Word(wor[0], wor[1]);
            word.add(neword);
        }
    }
    public void dictionaryLookup(){
        System.out.println("Tra từ: ");
        Scanner sc = new Scanner(System.in);
        String a = sc.nextLine();
        a = formWord(a);
        boolean check = false;
        int i = 0;
        while (!check && i < word.size()){
            Word neww = word.get(i);
            if(neww.getWord_target().equals(a)){
                System.out.println("nghĩa của từ(Tiếng Việt): ");
                System.out.println(neww.getWord_explain());
                check = true;
            } else if (neww.getWord_explain().equals(a)) {
                System.out.println("nghĩa của từ(Tiếng Anh): ");
                System.out.println(neww.getWord_target());
                check = true;
            } else {
                i++;
            }
        }
        if(i == word.size()){
            System.out.println("Chưa có từ này.");
        }
    }
    void dictionaryExportToFile() throws IOException {
        File file = new File("dictionaries");
        file.delete();
        OutputStream outputStream = new FileOutputStream(file);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);

        for(int i = 0; i < word.size(); i++){
            outputStreamWriter.write(word.get(i).getWord_target());
            outputStreamWriter.write("   ");
            outputStreamWriter.write(word.get(i).getWord_explain());
            outputStreamWriter.write("\n");
        }

        outputStreamWriter.flush();
    }
}
