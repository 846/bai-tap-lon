import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class DictionaryCommandline extends DictionaryManagement {
    public void showAllWords(){
        System.out.println("Từ điển \n");
        System.out.println("No   |English        |Vietnamese" + "\n");
        sortWord();
        for (int i = 0; i < word.size(); i++) {
            Word a = word.get(i);
            System.out.println(i + 1 + "   | " + a.getWord_target() + "           |" + a.getWord_explain());
        }
    }

    public void dictionaryBasic(){
            insertFromCommandline();
            showAllWords();
    }
    public void dictionaryAdvanced() throws IOException {
        insertFromFile();
        showAllWords();
        dictionaryLookup();
    }
    boolean miniSearch(String a, String b){
        int sum =0;
        boolean s = false;
        for(int i = 0; i<a.length(); i++){
            if(a.charAt(i) == b.charAt(0)){
                for(int j = i; j<b.length(); j++){
                    if(a.charAt(j) == b.charAt(j-i)){
                        sum++;
                    }
                }
                if(sum == b.length()){
                    s = true;
                    break;
                }
            }
        }
        return s;
    }
    void dictionarySearcher(){
        System.out.println("Nhập từ cần tìm: ");
        Scanner sc = new Scanner(System.in);
        String b = sc.next();
        b = formWord(b);
        int check = 0;
        for(int i = 0; i< word.size(); i++){
            String a = word.get(i).getWord_target();
            boolean s = false;
            s = miniSearch(a, b);
            if(s){
                check ++;
                System.out.println(word.get(i).getWord_target()+ "   |" + word.get(i).getWord_explain() + "\n");
            }
        }
        if(check == 0){
            System.out.println("Không có từ này.");
        }
    }
}
