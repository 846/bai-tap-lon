import java.util.ArrayList;

public class Dictionary {
    public static ArrayList<Word> word = new ArrayList<Word>();
    public String formWord(String word){
        char[] charArray = word.toCharArray();
        if (charArray[0] >= 97 && charArray[0] <= 122) {
            charArray[0] -= 32;
        }
        for(int i = 1; i<charArray.length; i++){
            if(charArray[i] >=65 && charArray[i] <= 90){
                charArray[i] += 32;
            }
        }
        word = String.valueOf(charArray);
        return word;
    }
    public void Swap(Word a, Word b){
        String atarget = a.getWord_target();
        String aexplain = a.getWord_explain();
        a.setWord_target(b.getWord_target());
        a.setWord_explain(b.getWord_explain());
        b.setWord_target(atarget);
        b.setWord_explain(aexplain);
    }
    public void sortWord(){
        for(int i = 0; i < word.size(); i++){
            for(int j = i+1; j< word.size(); j++){
                if(word.get(j).getWord_target().charAt(0) < word.get(i).getWord_target().charAt(0)){
                    Swap(word.get(i), word.get(j));
                } else if (word.get(j).getWord_target().charAt(0) == word.get(i).getWord_target().charAt(0)) {
                    if(word.get(i).getWord_target().length() > word.get(j).getWord_target().length()){
                        Swap(word.get(i), word.get(j));
                    }
                }
            }
        }
    }
}
